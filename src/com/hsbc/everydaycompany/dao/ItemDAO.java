package com.hsbc.everydaycompany.dao;
import java.util.*;
import com.hsbc.everydaycompany.model.*;
import com.hsbc.everydaycompany.exception.*;

/**
 * 
 * @author simran
 *purpose : the Dao Interface
 */

public interface ItemDAO {

	Item saveItemDetails(Item item);
	
	Item updateItemDetails(long itemCode, Item item);
	
	public void deleteItemDetails(long itemCode);
	
	List<Item> fetchAllItems();
	
	Item fetchItembyCode(long itemCode) throws ItemNotFound;

}

package com.hsbc.everydaycompany.dao;
import java.util.*;
import com.hsbc.everydaycompany.model.*;
import com.hsbc.everydaycompany.exception.*;

/**
 * 
 * @author simran
 *purpose : the class for the DAO implementation
 */

public class ListBackedItemDAO  implements ItemDAO{
	
	private List<Item> items = new LinkedList<>();

	@Override
	public Item saveItemDetails(Item item) {
		// TODO Auto-generated method stub
		this.items.add(item);
		return item;
	}

	@Override
	public Item updateItemDetails(long itemCode, Item item) {
		// TODO Auto-generated method stub
		for(Item it : items) {
			if(it.getItemCode() == itemCode) {
				it = item;
			}
		}return item;
	}

	@Override
	public void deleteItemDetails(long itemCode) {
		// TODO Auto-generated method stub
		for(Item it : items) {
			if(it.getItemCode() == itemCode) {
				this.items.remove(it);
			}
		}

	}

	@Override
	public List<Item> fetchAllItems() {
		// TODO Auto-generated method stub
		return items;
	}

	@Override
	public Item fetchItembyCode(long itemCode) throws ItemNotFound {
		// TODO Auto-generated method stub
		for(Item it : items) {
			if(it.getItemCode() == itemCode) {
				return it;
			}
		}throw new ItemNotFound("Item not found in the Records!!");

	}
	

}

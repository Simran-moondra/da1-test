package com.hsbc.everydaycompany.client;
import java.util.*;
import com.hsbc.everydaycompany.model.*;
import com.hsbc.everydaycompany.exception.*;
import com.hsbc.everydaycompany.service.*;

/**
 * 
 * @author simran
 *purpose : the Main Client Class
 */

public class ItemClient {
	
	public static void main(String[] args) {
		
		ItemService service = new ItemServiceImpl();
		
		FoodItem foodorder = new FoodItem(true);
		Apparel clothes = new Apparel("M", "Cotton");
		Electronics gadgets = new Electronics(12);
		
		FoodItem foodorder2 = new FoodItem(false);
		Apparel clothes2 = new Apparel("L", "Hosiery");
		Electronics gadgets2 = new Electronics(8);
		
		Item myOrder = service.createItemDetails("Milk",10, foodorder);
		
		Item myOrder2 = service.createItemDetails("Shirt",3, clothes);
		
		Item myOrder3 = service.createItemDetails("SmartPhone", 1, gadgets);
		
		System.out.println("Order details : \n" + "FoodOrder : \t" + myOrder + "\n Clothes Order : \t" +myOrder2 + "\n Electronics Order : \t" + myOrder3);
		
	}
	
	
	
	
	
	
	

}

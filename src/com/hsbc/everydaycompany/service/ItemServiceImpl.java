package com.hsbc.everydaycompany.service;
import java.util.*;
import com.hsbc.everydaycompany.model.*;
import com.hsbc.everydaycompany.dao.*;
import com.hsbc.everydaycompany.exception.*;

/**
 * 
 * @author simran
 *purpose : the class for the implementing the Service Interface
 */

public class ItemServiceImpl implements ItemService {
	
	private ItemDAO dao = new ListBackedItemDAO();

	@Override
	public Item createItemDetails(String itemName, int itemQuantity) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemQuantity);
		return this.dao.saveItemDetails(item);
	}
	
	@Override
	public Item createItemDetails(String itemName, int itemQuantity, FoodItem fooditem) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemQuantity, fooditem);
		return this.dao.saveItemDetails(item);
	}
	
	@Override
	public Item createItemDetails(String itemName, int itemQuantity, Apparel apparel) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemQuantity, apparel);
		return this.dao.saveItemDetails(item);
	}
	
	@Override
	public Item createItemDetails(String itemName, int itemQuantity, Electronics electronics) {
		// TODO Auto-generated method stub
		Item item = new Item(itemName, itemQuantity, electronics);
		return this.dao.saveItemDetails(item);
	}

	@Override
	public void deleteItemDetails(long itemCode) {
		// TODO Auto-generated method stub
		this.dao.deleteItemDetails(itemCode);
		
	}

	@Override
	public Item updateItemDetails(long itemCode, Item item) {
		// TODO Auto-generated method stub
		return this.dao.updateItemDetails(itemCode, item);
	}

	@Override
	public List<Item> fetchAllItems() {
		// TODO Auto-generated method stub
		return this.dao.fetchAllItems();
	}

	@Override
	public Item fetchItembyCode(long itemCode) throws ItemNotFound {
		// TODO Auto-generated method stub
		return this.dao.fetchItembyCode(itemCode);
	}

}

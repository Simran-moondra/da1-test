package com.hsbc.everydaycompany.service;
import java.util.*;
import com.hsbc.everydaycompany.model.*;
import com.hsbc.everydaycompany.exception.*;
import com.hsbc.everydaycompany.dao.*;

/**
 * 
 * @author simran
 *purpose : the Item Service Interface
 */

public interface ItemService {
	
	public Item createItemDetails(String itemName, int itemQuantity);
	
	public Item createItemDetails(String itemName, int itemQuantity, FoodItem fooditem);
	
	public Item createItemDetails(String itemName, int itemQuantity, Apparel apparel);
	
	public Item createItemDetails(String itemName, int itemQuantity, Electronics electronics);
	
	public void deleteItemDetails(long itemCode);
	
	public Item updateItemDetails(long itemCode, Item item);
	
	public List<Item> fetchAllItems();
	
	public Item fetchItembyCode(long itemCode) throws ItemNotFound;


}

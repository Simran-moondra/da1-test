package com.hsbc.everydaycompany.exception;

public class ItemNotFound extends Exception{
	
	public ItemNotFound(String message) {
		super(message);
	}
	
	public String getMessage() {
		return super.getMessage();
	}

}


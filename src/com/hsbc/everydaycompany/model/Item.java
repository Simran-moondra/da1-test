package com.hsbc.everydaycompany.model;
/**
 * 
 * @author simran
 * details - the main Item class
 *
 */

public class Item implements Comparable<Item> {

	private long itemCode;
	
	private String itemName;
	
	private int itemQuantity;
	
	private long counter = 100;
	
	private FoodItem fooditem;
	
	private Apparel apparel;
	
	private Electronics electronics;
	
	public Item(String itemName, int itemQuantity) {
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.itemCode = ++counter;
	}
	
	public Item(String itemName, int itemQuantity, FoodItem fooditem) {
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.fooditem = fooditem;
		this.itemCode = ++counter;
	}
	
	public Item(String itemName, int itemQuantity, Apparel apparel) {
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.apparel = apparel;
		this.itemCode = ++counter;
	}
	
	public Item(String itemName, int itemQuantity, Electronics electronics) {
		this.itemName = itemName;
		this.itemQuantity = itemQuantity;
		this.electronics = electronics;
		this.itemCode = ++counter;
	}
	
	
	
	
	public long getItemCode() {
		return itemCode;
	}

	public void setItemCode(long itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getItemQuantity() {
		return itemQuantity;
	}

	public void setItemQuantity(int itemQuantity) {
		this.itemQuantity = itemQuantity;
	}

	

	
	@Override
	public String toString() {
		return "Item [itemCode=" + itemCode + ", itemName=" + itemName + ", itemQuantity=" + itemQuantity + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (itemCode ^ (itemCode >>> 32));
		result = prime * result + ((itemName == null) ? 0 : itemName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (itemCode != other.itemCode)
			return false;
		if (itemName == null) {
			if (other.itemName != null)
				return false;
		} else if (!itemName.equals(other.itemName))
			return false;
		return true;
	}

	@Override
	public int compareTo(Item arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}

package com.hsbc.everydaycompany.model;

/**
 * 
 * @author simran
 *purpose : class for the Electronics section
 */

public class Electronics {
	
	int warranty;
	
	public Electronics(int warranty) {
		this.warranty = warranty;
	}

	public int getWarranty() {
		return warranty;
	}

	public void setWarranty(int warranty) {
		this.warranty = warranty;
	}

	
}

package com.hsbc.everydaycompany.model;

/**
 * 
 * @author simran
 * purpose : class for the Food items
 */

public class FoodItem {

	 boolean veg;
	
	public FoodItem (boolean veg) {
		this.veg = veg;
	}

	public boolean isVeg() {
		return veg;
	}

	public void setVeg(boolean veg) {
		this.veg = veg;
	}
	
	
}

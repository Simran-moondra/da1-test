package com.hsbc.everydaycompany.model;

/**
 * 
 * @author simran
 * purpose : class for the Apparels section
 */

public class Apparel {
	
	String size;
	
	String material;
	
	public Apparel(String size, String material) {
		this.size = size;
		this.material = material;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getMaterial() {
		return material;
	}

	public void setMaterial(String material) {
		this.material = material;
	}
	
	

}
